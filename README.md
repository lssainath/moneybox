# Moneybox Money Withdrawal

The solution contains a .NET core library (Moneybox.App) which is structured into the following 3 folders:

* Domain - this contains the domain models for a user and an account, and a notification service.
* Features - this contains two operations, one which is implemented (transfer money) and another which isn't (withdraw money)
* DataAccess - this contains a repository for retrieving and saving an account (and the nested user it belongs to)

## The task

The task is to implement a money withdrawal in the WithdrawMoney.Execute(...) method in the features folder. For consistency, the logic should be the same as the TransferMoney.Execute(...) method i.e. notifications for low funds and exceptions where the operation is not possible. 

As part of this process however, you should look to refactor some of the code in the TransferMoney.Execute(...) method into the domain models, and make these models less susceptible to misuse. We're looking to make our domain models rich in behaviour and much more than just plain old objects, however we don't want any data persistance operations (i.e. data access repositories) to bleed into our domain. This should simplify the task of implementing WidthdrawMoney.Execute(...).

## Guidlines

* You should spend no more than 1 hour on this task, although there is no time limit
* You should fork or copy this repository into your own public repository (Gihub, BitBucket etc.) before you do your work
* Your solution must compile and run first time
* You should not alter the notification service or the the account repository interfaces
* You may add unit/integration tests using a test framework (and/or mocking framework) of your choice
* You may edit this README.md if you want to give more details around your work (e.g. why you have done something a particular way, or anything else you would look to do but didn't have time)

Once you have completed your work, send us a link to your public repository.

Good luck!


# Solution


## Validation
	
Had we been using ASP.NET Core or some other framework like Web Api 2/MVC, I would have used custom validation attributes 
(or more likely) Fluent validation library and done all validation checks in validation classes, for the following reasons
1) It's good practice to have validation logic in separate model classes. 
	 By using Fluent validation we would have registered the service in the middleware pipeline (in asp.net core case) 
	 and the validation class would be called during various requests.
2) No additional dependencies need to be added to Model classes(like INotifications in this case). 
	 Since models are the core of the application we would not want them to be affected because of changes elsewhere
3) By moving the logic to validation classes we do not have to touch the models if any of the field validation rule changes in the future. 
	 Helps in following Open/Close principle 
	 
## Account class

I've changed the default constructor for this Account class. My assumption is that since the task was to modify the Account Model class this is an acceptable change. 
In a production scenario I would have discussed with the teams involed about changing the constructor and if I had to assume 
I would have gone for keeping the notification logic in the respective feature classes and just changing 
logic for limits in the Account domain class (which we do not know where else is being used, from just looking at the library code)

## Withdrawn field

The source of the Withdrawn field is unclear from the library and since I am simply following the logic of TransferMoney for WithdrawMoney, 
I am subtracting the amount we are subtracting from Balance.

I have consciously decided not to add any rules to the setter in the domain model. 
In a real life scenario I would interact with business/team and add any necessary rules based on our discussion.


## Other potential minor changes

public static read-only decimal PayInLimit;
PayInLimit could have been static readonly than being a const here. 
we could have then set the value in static constructor. So we could have gotten the value from some configuration file rather than hard coding it.

Adding validation rules to other Model classes as well like User (email, Name etc.)