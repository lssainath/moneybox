﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Moneybox.App;
using Moneybox.App.DataAccess;
using Moneybox.App.Domain.Services;
using Moneybox.App.Features;
using Moq;
using System;
using Xunit;

namespace Test.ModelTests
{
    public class WithdrawMoneyTest
    {
        private IFixture fixture;

        public WithdrawMoneyTest()
        {
            fixture = new Fixture()
                        .Customize(new AutoMoqCustomization());
        }
        [Fact]
        public void TestWithdrawMoney_With_Accpetable_Values()
        {
            var accountRepository = new Mock<IAccountRepository>();
            var notificationService = new Mock<INotificationService>();
            Account account = new Account(notificationService.Object);
            account.Balance = 1000m;
            accountRepository.Setup(a => a.GetAccountById(account.Id)).Returns(account);
            WithdrawMoney withdraw = new WithdrawMoney(accountRepository.Object, notificationService.Object);

            withdraw.Execute(account.Id, 100m);
            Assert.Equal(900m, account.Balance);
        }

        [Fact]
        public void TestWithdrawMoney_Trying_To_WithDraw_More_Than_Balance()
        {
            string email = "abc@ymail.com";
            var accountRepository = new Mock<IAccountRepository>();
            var notificationService = new Mock<INotificationService>();
            notificationService.Setup(n => n.NotifyFundsLow(email));
            Account account = new Account(notificationService.Object);
            account.User = new User() { Id = new Guid(), Email = email, Name = "ABC" };
            account.Balance = 10m;
            accountRepository.Setup(a => a.GetAccountById(account.Id)).Returns(account);
            WithdrawMoney withdraw = new WithdrawMoney(accountRepository.Object, notificationService.Object);

            Assert.Throws<InvalidOperationException>(() => withdraw.Execute(account.Id, 100m));
        }
    }
}
