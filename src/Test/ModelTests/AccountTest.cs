﻿using AutoFixture;
using AutoFixture.AutoMoq;
using Moneybox.App;
using System;
using Xunit;

namespace Test.ModelTests
{
    public class AccountTest
    {
        private IFixture fixture;
        public AccountTest()
        {
            fixture = new Fixture()
                        .Customize(new AutoMoqCustomization());
        }
        [Fact]
        public void AccountBalanceLessThanZeroError()
        {
            Account account = fixture.Create<Account>();
            Assert.Throws<InvalidOperationException>(() => account.Balance = -3);
        }

        [Fact]
        public void AccountPainIdGreaterThanlimit()
        {
            Account account = fixture.Create<Account>();
            Assert.Throws<InvalidOperationException>(() => account.PaidIn = Account.PayInLimit + 1);
        }

    }
}
