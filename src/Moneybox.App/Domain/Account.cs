﻿using Moneybox.App.Domain.Services;
using System;

namespace Moneybox.App
{
    public class Account
    {
        private readonly INotificationService _notificationService;
        private decimal _balance;
        private decimal _paidIn;

        public Account(INotificationService notificationService)
        {
            _notificationService = notificationService;
        }


        public const decimal PayInLimit = 4000m;

        public Guid Id { get; set; }

        public User User { get; set; }

        public decimal Balance
        {
            get
            {
                return _balance;
            }
            set
            {
                if (value < 0m)
                {
                    throw new InvalidOperationException("Insufficient funds to withdraw or make a transfer");
                }
                _balance = value;
                if (_balance < 500m)
                {
                    _notificationService.NotifyFundsLow(User.Email);
                }
            }
        }
        public decimal Withdrawn { get; set;}

        public decimal PaidIn
        {
            get
            {
                return _paidIn;
            }
            set
            {
                if (value > PayInLimit)
                {
                    throw new InvalidOperationException("Account pay in limit reached");
                }
                _paidIn = value;
                if (PayInLimit - PaidIn < 500m)
                {
                    _notificationService.NotifyApproachingPayInLimit(User.Email);
                }
            }
        }
    }
}
